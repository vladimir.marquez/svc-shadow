FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
WORKDIR /app
ARG GOOGLE_APPLICATION_CREDENTIALS=/app/.gcloud.json

COPY .gcloud.json /app/.gcloud.json
COPY pom.xml /app/pom.xml
RUN mvn dependency:go-offline
#RUN mvn verify clean --quiet -o
COPY ./ /app
RUN mvn package --quiet

FROM openjdk:11-slim
WORKDIR /app
COPY --from=MAVEN_BUILD /app/target/svc-shadow-0.0.1-SNAPSHOT.jar /app/svc-shadow.jar

RUN apt-get update && apt-get install curl unzip -y
RUN curl -O https://download.newrelic.com/newrelic/java-agent/newrelic-agent/current/newrelic-java.zip && unzip newrelic-java.zip && rm newrelic-java.zip && mv /app/newrelic/newrelic.jar /app/newrelic.jar && rm -rf /app/newrelic
COPY .newrelic/newrelic.yml /app/newrelic.yml

EXPOSE 8080
ENTRYPOINT ["java", "-javaagent:/app/newrelic.jar", "-jar", "/app/svc-shadow.jar"]
