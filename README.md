### Bootstrapping

##### Setup
- Create file called .env in root directory and copy the content of .env.example file into it

##### Install
- Install docker and docker-compose
- `$ docker-compose build`

### Run
- `$ docker-compose up`

You can access the status endpoint on
http://0.0.0.0:5000/api/status
for python or
http://0.0.0.0:8080/
for java



### Deploy

Each push generates a pipeline that builds the docker image and enables manual actions to deploy it into one of three environments:
- integration
- staging
- production

Once deployed, you can access the service in an environment (for example integration) by connecting to the cluster and using port-forwarding:

```bash
gcloud container clusters get-credentials retail-staging --zone us-central1-a --project buendolar
export NAMESPACE=svc-shadow
export POD_NAME=$(kubectl get pods --namespace $NAMESPACE -l "app.kubernetes.io/name=bb-svc,app.kubernetes.io/instance=svc-shadow" -o jsonpath="{.items[0].metadata.name}")
export CONTAINER_PORT=$(kubectl get pod --namespace $NAMESPACE $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
kubectl --namespace $NAMESPACE port-forward $POD_NAME 8080:$CONTAINER_PORT
```

and then visit
http://127.0.0.1:8080/
