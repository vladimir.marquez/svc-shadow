package com.buenbit.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
class SpringBootDockerApplicationTests {

	@Test
	void contextLoads() {
	}

}
